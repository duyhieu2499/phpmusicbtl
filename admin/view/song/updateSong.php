<?php
require_once('../../config/connect.php');
require('../../Model/Album.php');
require_once('../../Model/Genre.php');
require_once('../../Model/Singer.php');
require_once('../../Model/Song.php');

$id =$_GET["id"];
$song = new Song();
$id =$_GET["id"];
$getSong = $song->getSong($id);
// var_dump($getSinger);die();
  
if(isset($_POST["name"])) {
    $name =$_POST["name"];
    $artist_id =$_POST["artist_id"];
    $genre_id =$_POST["genre_id"];
    $album_id =$_POST["album_id"];
    $image =$_FILES['image'];
    $price =$_POST["price"];
    $getname=$song->updateSong($id,$name, $artist_id, $genre_id, $album_id, $image, $price); 
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  
</head>
<body>
<div class="container">
  <h2>Sửa bài hát</h2>
  <form action="" method ="POST" enctype="multipart/form-data">
    <div class="form-group">
      <label for="name">Tên bài hát</label>
      <input type="text" class="form-control" id="name" placeholder="" name="name" value="<?= $getSong["name"]?>">
    </div>

    <?php
    $singer = new Singer();
    $artists =$singer->getAll();
    $genre = new Genre();
    $genreNames = $genre->getAll();
    $album =new Album();
    $albumNames = $album->getAll();
    
    ?>
    <div class="form-inline">
        <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Ca sĩ</label>
        
        <select name="artist_id" class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref">
           <?php
           foreach($artists as $singer){
            if($singer["id"] == $getAlbum['artist_id']){
            ?>
            <option selected value="<?=$singer["id"]?>"><?=$singer["name"]?></option>

            <?php
            }
            else {
             ?>
             <option value="<?=$singer["id"]?>"><?=$singer["name"]?></option>
             <?php   
            }
           }
           ?>
          
        </select>
        
    </div>

    <div class="form-inline">
        <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Thể loại</label>
        <select name="genre_id" class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref">
        <?php
           foreach($genreNames as $genre){
            if($genre["id"]== $getGenre['genre_id']){
            ?>
            <option selected value="<?=$genre["id"]?>"><?=$genre["name"]?></option>

            <?php
            }
            else {
             ?>
             <option value="<?=$genre["id"]?>"><?=$genre["name"]?></option>
             <?php   
            }
           }
           ?>
            
        </select>
    </div>

    <div class="form-inline">
        <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Album</label>
        <select name="album_id" class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref">
        <?php
           foreach($albumNames as $album){
            if($album["id"]== $getAlbum['name']){
            ?>
            <option selected value="<?=$album["id"]?>"><?=$album["name"]?></option>

            <?php
            }
            else {
             ?>
             <option value="<?=$album["id"]?>"><?=$album["name"]?></option>
             <?php   
            }
           }
           ?>
            
        </select>
    </div>

    <div class="form-group">
        <label for="">Bài hát:</label>
        <input type="file" class="form-control" id="image" placeholder="" name="image">
        <audio controls>
            <source src="<?php echo "../../uploads/". $getSong['file_url']?>" type="audio/ogg">
        </audio>
    </div> 
    <div class="form-group">
      <label for="name">Giá</label>
      <input type="text" class="form-control" id="name" placeholder="" name="price" value="<?= $getSong["price"]?>">
    </div>

    
    <button type="submit" name="add" class="btn btn-primary">Thêm</button>
  </form>
 
</div>
</body>
</html>