<?php
require_once('../../config/connect.php');
require('../../Model/Album.php');
require_once('../../Model/Genre.php');
require_once('../../Model/Singer.php');

$id = $_GET["id"];
$album = new Album();
$id = $_GET["id"];


$getAlbum = $album->getAlbum($id);
// var_dump($getSinger);die();


if (isset($_POST["name"])) {
  $name = $_POST["name"];
  $artist_id = $_POST["artist_id"];
  $genre_id = $_POST["genre_id"];
  $description = $_POST["description"];
  $getname = $album->updateAlbum($id, $name, $artist_id, $genre_id, $description);
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>

<body>
  <div class="container">
    <h2>Sửa Album</h2>
    <form action="" method="POST" enctype="multipart/form-data">
      <div class="form-group">
        <label for="name">Tên album</label>
        <input type="text" class="form-control" id="name" placeholder="" name="name" value="<?= $getAlbum["name"] ?>">
      </div>

      <?php
      $singer = new Singer();
      $artists = $singer->getAll();
      $genre = new Genre();
      $genreNames = $genre->getAll();

      ?>
      <div class="form-inline">
        <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Ca sĩ</label>

        <select name="artist_id" class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref">
          <?php
          foreach ($artists as $singer) {
            if ($singer["id"] == $getAlbum['artist_id']) {
          ?>
              <option selected value="<?= $singer["id"] ?>"><?= $singer["name"] ?></option>

            <?php
            } else {
            ?>
              <option value="<?= $singer["id"] ?>"><?= $singer["name"] ?></option>
          <?php
            }
          }
          ?>

        </select>

      </div>

      <div class="form-inline">
        <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Thể loại</label>
        <select name="genre_id" class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref">
          <?php
          foreach ($genreNames as $genre) {
            if ($genre["id"] == $getAlbum['genre_id']) {
          ?>
              <option selected value="<?= $genre["id"] ?>"><?= $genre["name"] ?></option>

            <?php
            } else {
            ?>
              <option value="<?= $genre["id"] ?>"><?= $genre["name"] ?></option>
          <?php
            }
          }
          ?>

        </select>
      </div>

      <div class="mb-3">
        <label for="exampleFormControlTextarea1" class="form-label">Mô tả</label>
        <textarea class="form-control" id="exampleFormControlTextarea1" value="" name="description" rows="3"><?=$getAlbum["description"]?></textarea>
      </div>

      <button type="submit" name="add" class="btn btn-primary">Sửa</button>
    </form>

  </div>
</body>

</html>