<?php
require_once('../../config/connect.php');
require('../../Model/Singer.php');
if (isset($_POST["add"])) {
    include_once '../../Controller/SingerController.php';
    $create = new Singer();
    $result = $create->addSinger($_POST["name"], $_FILES['image']);
    // var_dump($_POST["name"]);
    header("Location: ../singer.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
  <h2>Thêm ca sĩ</h2>
  <form action="" method ="POST" enctype="multipart/form-data">
    <div class="form-group">
      <label for="name">Tên ca sĩ</label>
      <input type="text" class="form-control" id="name" placeholder="" name="name">
    </div>
    
    <div class="form-group">
      <label for="pwd">Image:</label>
      <input type="file" class="form-control" id="image" placeholder="" name="image" accept="image/*">
    </div>
   
    <button type="submit" name="add" class="btn btn-primary">Thêm</button>
  </form>
 
</div>
</body>
</html>