<?php
// require("../connect.php");
class UserController {
    protected $model;
    public function __construct($model){
        $this->model = $model;
    }

    // public function login()
    // {
    //     if (isset($_POST['submit'])) {
    //         $username = $_POST['username'];
    //         $password = $_POST['password'];

    //         $this->model->login($username, $password);
    //     }
    // }

    public function index() {
        // Display the login form
        require_once('../login.php');
      }

    public function login() {
        // Get the user input
        $user = 'default';
        $username = 'default';
        $password = 'default';
        if (isset($_POST['submit'])) {
        $username = $_POST['username'];
        $password = $_POST['password'];
        
        // Get the user from the database
        $user = $this->model->login($username, $password);
        //
        if ($user) {
            // Set the session variables
            $_SESSION['user_id'] = $user->id;
            $_SESSION['user_role'] = $user->role;
      
            // Redirect the user to the appropriate page
            if ($user->role == '1') {
              header('Location: admin/view/song.php');
            } else {
              header('Location: ../phpmusicbtl/customor/view/index.php');
            }
          } else {
            // Show an error message
            die('Invalid login');
          }
        }
        // Validate the input
        if (empty($username) || empty($password) || !$user) {
          // Show an error message
          die('Invalid login');
        }
        
      }

      public function add($username,$password, $email){
        $this->model->addUser($username,$password, $email);
    }

      public function update($id, $name, $username, $email, $password, $file){
        $this->model->updateUser($id, $name, $username, $email, $password, $file);
    }
}