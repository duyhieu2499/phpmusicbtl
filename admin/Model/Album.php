<?php
$dir = str_replace('Model', '', __DIR__);

require_once  $dir .'config/connect.php';
class Album{
    private $conn;
    public function __construct() {
        $this->conn = new DbConnect;
       $this->conn  = $this->conn->connect();
    }

    public function getAll(){
        $stmt = $this->conn->prepare("SELECT * FROM albums");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    public function addAlbum($name, $artist_id, $genre_id, $description) {
        $stmt = $this->conn->prepare("INSERT INTO albums (name, artist_id,genre_id,description) VALUES (:name, :artist_id, :genre_id, :description)");
        $stmt->bindParam(":name", $name);
        $stmt->bindParam(":artist_id", $artist_id );
        $stmt->bindParam(":genre_id", $genre_id);
        $stmt->bindParam(":description", $description);
        $stmt->execute();
        // $genre_id = $this->conn->lastInsertId();
        // return $genre_id;
    }

    public function updateAlbum($id,$name, $artist_id, $genre_id, $description){
        $stmt = $this->conn->prepare("UPDATE albums SET name = :name, artist_id = :artist_id, genre_id = :genre_id, description = :description  WHERE id = :id");
        
        $stmt->bindParam(":name", $name);
        $stmt->bindParam(":artist_id", $artist_id);
        $stmt->bindParam(":genre_id", $genre_id);
        $stmt->bindParam(":description", $description);
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        
         header("location:../album.php");
        
     }

     public function getAlbum($id){
        $stmt = $this->conn->prepare("select * from albums WHERE id = :id");
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result; 
     }

     public function delAlbum($id){
       
        $stmt = $this->conn->prepare("DELETE FROM albums WHERE id = :id");
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        return true;
    }

}
