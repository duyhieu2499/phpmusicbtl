<?php
// require("../connect.php");
class AlbumController {
    protected $model;
    public function __construct($model){
        $this->model = $model;
    }

     public function index(){
        $this->model->getAll();
    }

    public function add($name, $artist_id, $genre_id, $description){
        $this->model->addAlbum($name, $artist_id, $genre_id, $description);
    }

    public function update($id,$name, $artist_id, $genre_id, $description){
        $this->model->updateAlbum($id,$name, $artist_id, $genre_id, $description);
    }

    public function del($id){
        $this->model->delAlbum($id);
    }

}