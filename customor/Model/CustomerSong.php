<?php
$dir = str_replace('Model', '', __DIR__);

require_once  $dir . 'config/connect.php';
class Song
{
    private $conn;
    // public $rootAdmin = 'C:\laragon\www\phpmusicbtl\admin';
    public function __construct()
    {
        $this->conn = new DbConnect;
        $this->conn  = $this->conn->connect();
    }

    public function getAll()
    {
        $stmt = $this->conn->prepare("SELECT * FROM songs");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
}