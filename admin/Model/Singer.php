<?php
// require_once('../../helper.php');
$dir = str_replace('Model', '', __DIR__);

require_once  $dir .'config/connect.php';
class Singer{
    private $conn;
    private $addimage;
    public $rootAdmin = 'C:\laragon\www\phpmusicbtl\admin';
    public function __construct() {
        $this->conn = new DbConnect;
       $this->conn  = $this->conn->connect();
    }
    public function getAll(){
        $stmt = $this->conn->prepare("SELECT * FROM singers");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    public function addSinger($name,$file) {
        $stmt = $this->conn->prepare("INSERT INTO singers (name, image) VALUES (:name, :image)");
        $stmt->bindParam(":name", $name);
        $image_file = $_FILES["image"];

        // Image not defined, let's exit
        if (!isset($image_file)) {
            $image = '';
        }

        // Move the temp image file to the images/ directory
        move_uploaded_file(
            // Temp image location
            $file["tmp_name"],

            // New image location, __DIR__ is the location of the current PHP file
             "$this->rootAdmin/uploads/" . $file["name"]
        );
        $image = $file["name"];
        $stmt->bindParam(":image", $image);
        $stmt->execute();
        
        $singer_id = $this->conn->lastInsertId();
        return $singer_id;
    }

    public function updateSinger($id,$name,$file){
        $stmt = $this->conn->prepare("UPDATE singers SET name = :name, image = :image WHERE id = :id");
        $stmt->bindParam(":id", $id);
        $stmt->bindParam(":name", $name);
        $image_file = $_FILES["image"];

        // Image not defined, let's exit
        if (!isset($image_file)) {
            $image = '';
        }

        // Move the temp image file to the images/ directory
        $singer = $this->getSinger($id);
        unlink('../../uploads/'.$singer['image']);
        move_uploaded_file(
            // Temp image location
            $file["tmp_name"],

            // New image location, __DIR__ is the location of the current PHP file
             "$this->rootAdmin/uploads/" . $file["name"]
        );
        $image = $file["name"];
        $stmt->bindParam(":image", $image);
        $stmt->execute();
        
        // $singer_id = $this->conn->lastInsertId();
        // return $singer_id;
        
         header("location:../singer.php");
        
     }

     public function getSinger($id){
        $stmt = $this->conn->prepare("select * from singers WHERE id = :id");
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result; 
     }

     public function delSinger($id){
        $singer = $this->getSinger($id);
        unlink('../uploads/'.$singer['image']);
        $stmt = $this->conn->prepare("DELETE FROM singers WHERE id = :id");
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        return true;
    }

}
