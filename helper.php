<?php
    function uploadImage($imageName, $imgTmp) {
        $target_dir = "uploads/";
        if (!is_dir($target_dir)) {
            mkdir($target_dir, 0777, true);
        }
        $imgName = time() . $imageName;
        $image = $target_dir . $imgName;
        move_uploaded_file($imgTmp, $image);
        return $image;
    }
?>