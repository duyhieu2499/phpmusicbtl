<?php
require_once('../../config/connect.php');
require('../../Model/Album.php');
require('../../Model/Singer.php');
require('../../Model/Genre.php');
//lay ten ca si va the loai
$singerModel= new Singer ();
$listSinger = $singerModel->getAll();
$genreModel = new Genre();
$listGenre = $genreModel->getAll();

if (isset($_POST["add"])) {
    include_once '../../Controller/AlbumController.php';
    $create = new Album();
   $result = $create->addAlbum($_POST["name"],$_POST['artist_id'], $_POST['genre_id'], $_POST['descreption']);
    header("Location: ../album.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  
</head>
<body>
<div class="container">
  <h2>Thêm Album</h2>
  <form action="" method ="POST" enctype="multipart/form-data">
    <div class="form-group">
      <label for="name">Tên album</label>
      <input type="text" class="form-control" id="name" placeholder="" name="name">
    </div>

    <div class="form-inline">
        <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Ca sĩ</label>
        
        <select name="artist_id" class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref">
            <option selected>Choose...</option>
            <?php
            foreach($listSinger as $nameSinger){
            ?>
            <option value="<?=$nameSinger['id']?>"><?=$nameSinger['name']?></option>
            <?php 
            }
            ?>
        </select>
        
    </div>

    <div class="form-inline">
        <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Thể loại</label>
        <select name="genre_id" class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref">
            <option selected>Choose...</option>
            <?php
            foreach($listGenre as $nameGenre){
            ?>
            <option value="<?=$nameGenre['id']?>"><?=$nameGenre['name']?></option>
            <?php 
            }
            ?>
        </select>
    </div>

    <div class="mb-3">
  <label for="exampleFormControlTextarea1" class="form-label">Mô tả</label>
  <textarea class="form-control" id="exampleFormControlTextarea1" name="descreption"rows="3"></textarea>
</div>
    
    <button type="submit" name="add" class="btn btn-primary">Thêm</button>
  </form>
 
</div>
</body>
</html>