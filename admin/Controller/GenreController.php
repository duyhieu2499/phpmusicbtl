<?php
// require("../connect.php");
class GenreController {
    protected $model;
    public function __construct($model){
        $this->model = $model;
    }

     public function index(){
        $this->model->getAll();
    }

    public function add($name){
        $this->model->addGenre($name);
    }

    public function update($id){
        $name = $_POST['name'];
        $this->model->updateGenre($id,$name);
    }

    public function del($id){
        $this->model->delGenre($id);
    }

}