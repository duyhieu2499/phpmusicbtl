<?php
$dir = str_replace('Model', '', __DIR__);

require_once  $dir .'config/connect.php';
class Genre{
    private $conn;
    public function __construct() {
        $this->conn = new DbConnect;
       $this->conn  = $this->conn->connect();
    }

    public function getAll(){
        $stmt = $this->conn->prepare("SELECT * FROM genres");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    public function addGenre($name) {
        $stmt = $this->conn->prepare("INSERT INTO genres (name) VALUES (:name)");
        $stmt->bindParam(":name", $name);
        $stmt->execute();
        $genre_id = $this->conn->lastInsertId();
        return $genre_id;
    }

    public function updateGenre($id,$name){
        $stmt = $this->conn->prepare("UPDATE genres SET name = :name WHERE id = :id");
        $stmt->bindParam(":name", $name);
        $stmt->bindParam(":id", $id);
        $excute = $stmt->execute();
        
         header("location:../genre.php");
        
     }

     public function getGenre($id){
        $stmt = $this->conn->prepare("select * from genres WHERE id = :id");
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result; 
     }

     public function delGenre($id){
       
        $stmt = $this->conn->prepare("DELETE FROM genres WHERE id = :id");
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        return true;
    }

}
