<?php
class SongController {
    protected $model;
    public function __construct($model){
        $this->model = $model;
    }

     public function index(){
        $this->model->getAll();
    }

    public function add($name, $artist_id, $genre_id, $album_id, $file, $price){
        $this->model->addSong($name, $artist_id, $genre_id, $album_id, $file, $price);
    }

    public function update($id, $name, $artist_id, $genre_id, $album_id, $file, $price){
        // $name = $_POST['name'];
        $this->model->updateSong($id, $name, $artist_id, $genre_id, $album_id, $file, $price);
    }

    public function del($id){
        $this->model->delSong($id);
    }

}