<?php
// require("../connect.php");
class SingerController {
    protected $model;
    public function __construct($model){
        $this->model = $model;
    }

     public function index(){
        $this->model->getAll();
    }

    public function add($name,$image){
        $this->model->addSinger($name,$image);
    }

    public function update($id,$name,$image){
        $name = $_POST['name'];
        $image = $_FILES['image'];
        $this->model->updateSinger($id,$name,$image);
    }

    public function del($id){
        $this->model->delSinger($id);
    }

}