<?php
class User{
    private $conn;
    public $rootAdmin = 'C:\laragon\www\phpmusicbtl\admin';

    public function __construct() {
        $this->conn = new DbConnect;
       $this->conn  = $this->conn->connect();
    }

    public function login($username, $password)
    {
        $sql = "SELECT * FROM users where username = :username AND password = :password";
        // var_dump($sql);
        $query = $this->conn->prepare($sql);
        $query->bindParam(':username', $username);
        $query->bindParam(':password', $password);
        $query->execute();
        return $query->fetch(PDO::FETCH_OBJ);

        // if ($query->rowCount() == 1) {
        //     header('location: index.php');
        // } else {
        //     header('location: test.php');
        // }
    }

    public function addUser($username, $email, $password)
    {

        $stmt = $this->conn->prepare(
            "INSERT INTO users (username,email,password) VALUES (:username,:email,:password)");
        $stmt->bindParam(":username", $username);
        $stmt->bindParam(":email", $email);
        $stmt->bindParam(":password", $password);
        $stmt->execute();

        $user_id = $this->conn->lastInsertId();
        return $user_id;
    }

    public function updateUser($id, $username, $email, $password, $file)
    {
        $stmt = $this->conn->prepare("UPDATE users SET username = :username, email = :email, avatar = :avatar, password = :password WHERE id = :id");
        $stmt->bindParam(":id", $id);
        $stmt->bindParam(":username", $username);
        $stmt->bindParam(":email", $email);
        $stmt->bindParam(":password", $password);
        //$stmt->bindParam(":avatar", $avatar);
        $image_file = $_FILES["image"];

        // Image not defined, let's exit
        if (!isset($image_file)) {
            $image = '';
        }

        // Move the temp image file to the images/ directory
        $user = $this->getUser($id);
        unlink('admin/uploads/' . $user['avatar']);
        //var_dump('admin/uploads/' . $user['avatar']);
        move_uploaded_file(
            // Temp image location
            $_FILES['image']["tmp_name"],

            // New image location, __DIR__ is the location of the current PHP file
            "$this->rootAdmin/uploads/" . $_FILES['image']["name"]
        );
        $image = $file;
        $stmt->bindParam(":avatar", $image);
        $stmt->execute();

        header("location:../phpmusicbtl/admin/view/song.php");
    }



    public function getUser($id)
    {
        $stmt = $this->conn->prepare("select * from users WHERE id = :id");
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

}
