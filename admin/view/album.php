
<?php
require_once('../Model/Album.php');
require_once('../Model/Singer.php');
require_once('../Model/Genre.php');
$album = new Album();
if(isset($_GET['id'])) {
  $id = $_GET['id'];
  $del = $album->delAlbum($id);
}

?>

<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Hope UI | Responsive Bootstrap 5 Admin Dashboard Template</title>
      
      <!-- Favicon -->
      <link rel="shortcut icon" href="../../assets/images/favicon.ico" />
      
      <!-- Library / Plugin Css Build -->
      <link rel="stylesheet" href="../../assets/css/core/libs.min.css" />
      
      
      <!-- Hope Ui Design System Css -->
      <link rel="stylesheet" href="../../assets/css/hope-ui.min.css?v=1.2.0" />
      
      <!-- Custom Css -->
      <link rel="stylesheet" href="../../assets/css/custom.min.css?v=1.2.0" />
      
      <!-- Dark Css -->
      <link rel="stylesheet" href="../../assets/css/dark.min.css"/>
      
      <!-- Customizer Css -->
      <link rel="stylesheet" href="../../assets/css/customizer.min.css" />
      
      <!-- RTL Css -->
      <link rel="stylesheet" href="../../assets/css/rtl.min.css"/>
      
  </head>
  <body class="  ">
    <!-- loader Start -->
    <div id="loading">
      <div class="loader simple-loader">
          <div class="loader-body"></div>
      </div>    </div>
    <!-- loader END -->
    
  <?php
   include_once('aside.php');
  ?>
    <main class="main-content">
      <div class="position-relative iq-banner">
        <!--Nav Start-->
        <?php
        include_once('nav.php');
        ?>
       <div class="iq-navbar-header" style="height: 215px;">
              <div class="container-fluid iq-container">
                  <div class="row">
                      <div class="col-md-12">
                          <div class="flex-wrap d-flex justify-content-between align-items-center">
                              <div>
                                  <h1>Hello Devs!</h1>
                                  <p>We are on a mission to help developers like you build successful projects for FREE.</p>
                              </div>
                              <div>
                                  <a href="album/addAlbum.php" class="btn btn-link btn-soft-light btn-success">
                                      Thêm
                                  </a>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="iq-header-img">
                  <img src="../../assets/images/dashboard/top-header.png" alt="header" class="theme-color-default-img img-fluid w-100 h-100 animated-scaleX">
                  <img src="../../assets/images/dashboard/top-header1.png" alt="header" class="theme-color-purple-img img-fluid w-100 h-100 animated-scaleX">
                  <img src="../../assets/images/dashboard/top-header2.png" alt="header" class="theme-color-blue-img img-fluid w-100 h-100 animated-scaleX">
                  <img src="../../assets/images/dashboard/top-header3.png" alt="header" class="theme-color-green-img img-fluid w-100 h-100 animated-scaleX">
                  <img src="../../assets/images/dashboard/top-header4.png" alt="header" class="theme-color-yellow-img img-fluid w-100 h-100 animated-scaleX">
                  <img src="../../assets/images/dashboard/top-header5.png" alt="header" class="theme-color-pink-img img-fluid w-100 h-100 animated-scaleX">
              </div>
          </div>          <!-- Nav Header Component End -->
        <!--Nav End-->
      </div>
      <div class="conatiner-fluid content-inner mt-n5 py-0">
      <div class="row">                  
         <div class="col-lg-12">
            <div class="card   rounded">
               <div class="card-body">
                 
                  <div class="row">
                     <div class="col-sm-12 mt-4">
                        <div class="table-responsive-lg">
                           <table class="table">
                              <thead>
                                 <tr>
                                    <th class="text-center" scope="col">#</th>
                                    <th class="text-center" scope="col">Tên Album</th>
                                    <th class="text-center" scope="col">Ca sĩ</th>
                                    <th class="text-center" scope="col">Thể loại</th>
                                    <th class="text-center" scope="col">Mô tả</th>
                                    <th class="text-center" scope="col">Action</th>
                                    
                                 </tr>
                              </thead>
                              <tbody>
                              <?php
                                  $getAlbum= $album->getAll();
                                  foreach ($getAlbum as $key=>$row) {  
                                    $singer = new Singer();
                                    $artist =$singer->getSinger($row['artist_id']);
                                    $genre = new Genre();
                                    $genreName = $genre->getGenre($row['genre_id']);
                              ?>
                                 <tr>
                                    <td><?= $key+1?></td>
                                    <td class="text-center"><?= $row['name']?></td>
                                    <td class="text-center"><?= $artist['name']?></td>
                                    <td class="text-center"><?= $genreName['name']?></td>
                                    <td class="text-center"><?= $row['description']?></td>
                                    <td class="text-center">
                                    <button class="btn btn-warning"><a href="album/updateAlbum.php?id=<?php echo $row['id']?>">Sửa</a></button>
                                        <a class="btn btn-danger" href="?id=<?php echo $row['id']?>">Xóa</a>
                                    </td>
                                 </tr>
                                <?php
                                  }
                                ?>
                                 
                              </tbody>
                           </table>
                        </div>
                     </div>                              
                  </div>
                  
               </div>
            </div>
         </div>                                    
      </div>
      </div>
      
      <!-- Footer Section End -->    
    </main>
 
    <!-- Wrapper End-->
    <!-- offcanvas start -->
   <!-- Library Bundle Script -->
    <script src="../../assets/js/core/libs.min.js"></script>
    
    <!-- External Library Bundle Script -->
    <script src="../../assets/js/core/external.min.js"></script>
    
    <!-- Widgetchart Script -->
    <script src="../../assets/js/charts/widgetcharts.js"></script>
    
    <!-- mapchart Script -->
    <script src="../../assets/js/charts/vectore-chart.js"></script>
    <script src="../../assets/js/charts/dashboard.js" ></script>
    
    <!-- fslightbox Script -->
    <script src="../../assets/js/plugins/fslightbox.js"></script>
    
    <!-- Settings Script -->
    <script src="../../assets/js/plugins/setting.js"></script>
    
    <!-- Slider-tab Script -->
    <script src="../../assets/js/plugins/slider-tabs.js"></script>
    
    <!-- Form Wizard Script -->
    <script src="../../assets/js/plugins/form-wizard.js"></script>
    
    <!-- AOS Animation Plugin-->
    
    <!-- App Script -->
    <script src="../../assets/js/hope-ui.js" defer></script>
  </body>
</html>