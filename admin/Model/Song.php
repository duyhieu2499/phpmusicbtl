<?php
$dir = str_replace('Model', '', __DIR__);

require_once  $dir . 'config/connect.php';
class Song
{
    private $conn;
    public $rootAdmin = 'C:\laragon\www\phpmusicbtl\admin';
    public function __construct()
    {
        $this->conn = new DbConnect;
        $this->conn  = $this->conn->connect();
    }

    public function getAll()
    {
        $stmt = $this->conn->prepare("SELECT * FROM songs");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    public function addSong($name, $artist_id, $genre_id, $album_id, $file, $price)
    {

        $stmt = $this->conn->prepare(
            "INSERT INTO songs (
            name,
            artist_id,
            genre_id,
            album_id,
            file_url,
            price
            ) 
            VALUES (
                :name,
                :artist_id,
                :genre_id,
                :album_id,
                :file_url,
                :price)"
        );
        $stmt->bindParam(":name", $name);
        $stmt->bindParam(":artist_id", $artist_id);
        $stmt->bindParam(":genre_id", $genre_id);
        $stmt->bindParam(":album_id", $album_id);
        $stmt->bindParam(":file_url", $file['name']);
        $stmt->bindParam(":price", $price);
        $image_file = $_FILES["image"];

        // Image not defined, let's exit
        if (!isset($image_file)) {
            $image = '';
        }

        // Move the temp image file to the images/ directory
        move_uploaded_file(
            // Temp image location
            $file["tmp_name"],

            // New image location, __DIR__ is the location of the current PHP file
            "$this->rootAdmin/uploads/" . $file["name"]
        );
        // $image = $file["name"];
        // // $stmt->bindParam(":image", $image);
        $stmt->execute();

        $song_id = $this->conn->lastInsertId();
        return $song_id;
    }

    public function updateSong($id, $name, $artist_id, $genre_id, $album_id, $file, $price)
    {
        $stmt = $this->conn->prepare("UPDATE songs SET name = :name,artist_id = :artist_id,genre_id = :genre_id,album_id = :album_id, file_url = :file_url, price = :price WHERE id = :id");
        $stmt->bindParam(":id", $id);
        $stmt->bindParam(":name", $name);
        $stmt->bindParam(":artist_id", $artist_id);
        $stmt->bindParam(":genre_id", $genre_id);
        $stmt->bindParam(":album_id", $album_id);
        // $stmt->bindParam(":image", $name);
        $stmt->bindParam(":price", $price);
        $image_file = $_FILES["image"];

        // Image not defined, let's exit
        if (!isset($image_file)) {
            $image = '';
        }

        // Move the temp image file to the images/ directory
        $song = $this->getSong($id);
        unlink('../../uploads/' . $song['image']);
        move_uploaded_file(
            // Temp image location
            $file["tmp_name"],

            // New image location, __DIR__ is the location of the current PHP file
            "$this->rootAdmin/uploads/" . $file["name"]
        );
        $image = $file["name"];
        $stmt->bindParam(":file_url", $image);
        $stmt->execute();

        header("location:../song.php");
    }

    public function getSong($id)
    {
        $stmt = $this->conn->prepare("select * from songs WHERE id = :id");
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public function delSong($id)
    {
        $stmt = $this->conn->prepare("DELETE FROM songs WHERE id = :id");
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        return true;
    }
}
